/**
 * Adds two numbers together
* @param {number} a
* @param {number} b
* @returns {number}
*/
const add = (a, b) => a + b;

/** 
 * subtracts number from minuend
 * @param {number} minuend
 * @param {number} subtrahend
 * @returns {number} difference
*/
const subtract = (minuend, subtrahend) => {
    return minuend-subtrahend;
}

const multiply = (a, b) => a * b;

/**
 * Divides two numbers
 * @param {number} dividend
 * @param {number} divisor
 * @returns {number}
 * @throws {Error}
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed")
    const fraction = dividend / divisor;
    return fraction;
}
export default {add, subtract, multiply, divide}
