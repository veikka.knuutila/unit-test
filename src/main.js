import express from 'express';
import calc from './calc.js';

const app = express();
const port = 3000;
const host = "localhost";

app.get('/', (req, res) => {
    res.status(200).send('Hello world');
});

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = calc.add(a, b);
    res.status(200).send(sum.toString());
});

app.listen(port, host, () => {
    console.log(`http://${host}:${port}`)
})